#include <stdio.h>
#include <stdlib.h>

void sort_array( int a[], const int n );

int main( )
{
	
	int a[] = { 7, 8, 3, 2, 6, 4, 2, 4, 5, 0, 2, -8 };
	int x;
	sort_array( a, x = ( sizeof a/ sizeof a[0] ) );

	for ( int i = 0; i < x; i++ )
		printf( "Element [%d] is %d.\n", i, a[ i ] );

	puts( "Program complete." );

	return EXIT_SUCCESS;
}



void sort_array( int a[], const int n ) 
{
        int holdit, holdit_spot;
	int i, j;

        for ( i = 0; i < n; i++ ) { 
                holdit = a[ i ];
		holdit_spot = i;
                for ( j = i + 1; j < n; j++ ) { 
                        if ( holdit > a[ j ] ) { 
                                holdit = a[ j ];
                                holdit_spot = j;
                        }   
                }   
                // holdit has the lowest value in the array.
                a[ holdit_spot ] = a[ i ];
                a[ i ] = holdit; 
        }   

	printf( "The value of i and j is ( %d, %d )\n", i, j );

}




