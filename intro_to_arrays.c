#include <stdio.h>
#include <stdlib.h>

#define END_OF_USER_INPUT -1
#define MAXIMUM_NUMBERS_IN_ARRAY 100

int get_a_number( void );
void print_int_array( const int my_numbers[ ], const int my_index );
int my_mode( const int my_numbers[ ], const int my_index );
int calculate_sum( const int a[], const int count );
void produce_summary_statistics( char *announcement, const int s[], const int s_count );
void sort_array( int a[], const int n );



int main( )
{
	int numin = 0;
	int sum = 0;
	int my_numbers[ MAXIMUM_NUMBERS_IN_ARRAY ];
	int my_index = 0;

	int sample1[ ] = { 3, 5, 6, 7, 7, 5, 5, 5 };
	int sample1_count = 8;

	int sample2[ ] = { 10, 20, 30, 20, 10, 20, 30, 30, 30, 20, 30 };
	int sample2_count = sizeof sample2 / sizeof sample2[ 0 ];

	int sample3[ ] = { 2, 1, 4, 5, 6, 3, 7, 8, 10, 12, 15, 20, 9 };
	int sample3_count = sizeof sample3 / sizeof sample3[ 0 ];

	produce_summary_statistics( "\nSample 1 ------------------------", sample1, sample1_count );
	produce_summary_statistics( "\nSample 2 ------------------------", sample2, sample2_count );
	produce_summary_statistics( "\nSample 3 ------------------------", sample3, sample3_count );

	puts( "Program complete." );

	return EXIT_SUCCESS;
}



int my_mode( const int my_numbers[ ], const int my_index )
{
	int max_mode_count = 0, freq_val;
	int i, j, mode_count;

	// Go through all the numbers in the array.
	for ( i = 0; i < my_index; i++ ) {
		
		// set to 1 for the number we're currently evaluating appearing once.
		mode_count = 1;  

		// go through all numbers from i+1 to the end of the array.
		for ( j = i + 1; j < my_index; j++ )
			if ( my_numbers[ i ] == my_numbers[ j ] )
				mode_count++;

		// if our count for this number is greater than the previous max mode, update.
		if ( mode_count > max_mode_count ) {
			max_mode_count = mode_count;
			freq_val = my_numbers[ i ];
		}
	}
	return freq_val; 
}



/* 
 * Function: 		print_int_array
 * Precondtions: 	my_numbers is an integer array.
 *                      my_index is the count of numbers in the array.
 * Postcondtions:  	* None *
 * Globals:		* None *
 * Returns:		* Nothing *
 */

void print_int_array( const int my_numbers[ ], const int my_index ) {
	int i;
	int sum = 0; 

	int new_arr[ MAXIMUM_NUMBERS_IN_ARRAY ];

	for ( i = 0; i < my_index; i++ )
		new_arr[ i ]  = my_numbers[ i ];

	sort_array( new_arr, my_index );

	printf( "The following %d values have been read:\n", my_index );
	for ( i = 0; i < my_index; i++ )
		printf( "    Number %d = %d.  (sum:%d)\n", i, new_arr[ i ], sum += new_arr[ i ] );

	return;
}



int get_a_number( void ) {
	int numin;

	printf( "Enter a postive number or %d to quit: ", END_OF_USER_INPUT );
	scanf( "%d", &numin );

	return numin;
}



int calculate_sum( const int a[], const int count )
{
	int i;
	int sum = 0;
	for ( i = 0; i < count; i++ )
		sum += a[ i ];
	return sum;
}



void produce_summary_statistics( char *announcement, const int s[], const int s_count )
{
	// Show the program user what numbers we have.
	puts( announcement );
	print_int_array( s, s_count );
	int sample1_sum = calculate_sum( s, s_count );

	// Output results from the numbers.
	printf( "The sum of the numbers you entered is %d.\n", sample1_sum );
	printf( "The average of the numbers is %lf.\n", sample1_sum / (double)s_count );
	printf( "The mode of those numbers is %d.\n", my_mode( s, s_count ) );

	return;
}



void sort_array( int a[], const int n ) 
{
        int i, j, holdit, holdit_spot;

        for ( i = 0; i < n; i++ ) { 
                holdit = a[ i ];
                holdit_spot = i;
                for ( j = i + 1; j < n; j++ ) { 
                        if ( holdit > a[ j ] ) { 
                                holdit = a[ j ];
                                holdit_spot = j;
                        }   
                }   
                // holdit has the lowest value found in the array.
                a[ holdit_spot ] = a[ i ];
                a[ i ] = holdit; 
        }   
}